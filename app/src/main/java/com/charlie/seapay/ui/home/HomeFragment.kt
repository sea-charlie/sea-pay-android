package com.charlie.seapay.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.charlie.seapay.R
import com.charlie.seapay.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(
            inflater, R.layout.fragment_home, container, false
        )

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        homeViewModel.navigateToTopUp.observe(this, Observer {
            if( it == true ){
                Toast.makeText(activity, "Top Up", Toast.LENGTH_SHORT).show()
            }
            homeViewModel.doneNavigating()
        })

        homeViewModel.navigateToTransfer.observe(this, Observer {
            if( it == true ){
                Toast.makeText(activity, "Transfer", Toast.LENGTH_SHORT).show()
            }
            homeViewModel.doneNavigating()
        })

        homeViewModel.navigateToRewards.observe(this, Observer {
            if( it == true ){
                Toast.makeText(activity, "Rewards", Toast.LENGTH_SHORT).show()
            }
            homeViewModel.doneNavigating()
        })

        binding.homeViewModel = homeViewModel

        binding.lifecycleOwner = this

        return binding.root
    }


}
