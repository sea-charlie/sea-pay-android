package com.charlie.seapay.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.charlie.seapay.util.IndonesiaCurrency
import timber.log.Timber
import java.math.BigDecimal
import java.text.NumberFormat

class HomeViewModel: ViewModel() {

    private val _balance = MutableLiveData<BigDecimal>()
    val balance : LiveData<String> = Transformations.map(_balance){
        IndonesiaCurrency.valueOf(it)
    }

    private val _navigateToTopUp = MutableLiveData<Boolean>()
    val navigateToTopUp : LiveData<Boolean>
        get() = _navigateToTopUp

    private val _navigateToTransfer = MutableLiveData<Boolean>()
    val navigateToTransfer : LiveData<Boolean>
        get() = _navigateToTransfer

    private val _navigateToRewards = MutableLiveData<Boolean>()
    val navigateToRewards
        get() = _navigateToRewards

    init {
        Timber.i("HomeViewModel created")

        //TODO:: get balance from repo
        _balance.value = BigDecimal.valueOf(10000000)
    }

    //TODO:: navigate
    fun onTopUpClicked(){
        _navigateToTopUp.value = true
    }

    //TODO:: navigate
    fun onTransferClicked(){
        _navigateToTransfer.value = true
    }

    //TODO:: navigate
    fun onRewardsClicked(){
        _navigateToRewards.value = true
    }

    fun doneNavigating(){
        if( _navigateToTopUp.value == true ){
            _navigateToTopUp.value = null
        }else if ( _navigateToTransfer.value == true ){
            _navigateToTransfer.value = null
        }else if( _navigateToRewards.value == true ){
            _navigateToRewards.value = null
        }
    }

    override fun onCleared() {
        super.onCleared()

        Timber.i("HomeViewModel destroyed")
    }
}