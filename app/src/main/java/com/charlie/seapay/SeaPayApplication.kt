package com.charlie.seapay

import android.app.Application
import timber.log.Timber

class SeaPayApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}