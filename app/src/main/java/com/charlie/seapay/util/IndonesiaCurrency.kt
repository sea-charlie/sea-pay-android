package com.charlie.seapay.util

import java.math.BigDecimal
import java.math.BigInteger
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

class IndonesiaCurrency {

    companion object{
        fun valueOf(amount: BigDecimal): String? {
            val localeID = Locale("in", "ID")

            val formatRp = NumberFormat.getCurrencyInstance(localeID)
            formatRp.maximumFractionDigits = 0

            return formatRp.format(amount)
        }
    }
}